/**
 * 
 */
package com.abacus.dummyapi;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertTrue;

import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.path.json.exception.JsonPathException;
import io.restassured.response.Response;


/**
 * @author tejas_marathe
 * This class contains test cases for Update operations on Dummy API
 * Following tests are covered:
 * Positive Tests
 * 1. Update Records with following data
 *    - Update one field, multiple fields and all fields of the record
 *    - Update the same record twice with same data
 * Negative Tests
 * 1. Attempt to update record for following data
 *    - Missing, misspelled, extra attribute values
 *    - ID field - unique key in the database
 *    - Blank, negative and alphanumeric values for salary, age fields
 *    - Floating point value for age
 * 
 * It uses @BeforeMethod and @AfterMethod (inherited from BaseTest) and @BeforeClass TestNG annotations to set up HTML reporting using ExtentReports
 * Each test method annotated by @Test contains test description, success and failure messages, api calls and assertions
 */


public class UpdateTests extends BaseTest {

	private String employeeId = "";
	private String origEmployeeName = "";
	private String origEmployeeSalary = "";
	private String origEmployeeAge = "";
	
	private static final Logger LOG = Logger.getLogger(UpdateTests.class);
	
	@BeforeClass
	public void SetupUpdateTestCases() {
		report = new ExtentReports(Paths.get("").toAbsolutePath().toString() + "\\src\\test\\resources\\results\\APIUpdateTestResults.html");
		LOG.debug("Creating initial record before running update test cases.");
		Response CreateEmployeeResponse = CreateRecord("Update Test Values", "TestUser" + System.currentTimeMillis() + Math.random(), "1000", "100");
		employeeId = CreateEmployeeResponse.body().jsonPath().getString("id").toString();
		LOG.info("Employee ID: " + employeeId);
		if (employeeId.equals("")) {
			reportLog.log(LogStatus.FATAL, "Could not create employee before starting Update operation test cases. Exiting...");
			assertTrue(false);
		}
		origEmployeeName = CreateEmployeeResponse.body().jsonPath().getString("name");
		origEmployeeSalary = CreateEmployeeResponse.body().jsonPath().getString("salary");
		origEmployeeAge = CreateEmployeeResponse.body().jsonPath().getString("age");
	}

	@Test(enabled=true, dataProvider="PositiveDataProvider")
	public void UpdateEmployeeTest(String testScenario, String employeeName, String employeeSalary, String employeeAge) {
		LOG.info("Running Scenario: " + testScenario);	
		
		String employeeData = "{\"name\" : \"" + employeeName + "\", \"salary\" : \"" + employeeSalary + "\", \"age\" : \"" + employeeAge + "\"}";
		
		
		reportLog.setDescription("Verify employee record is updated successfully for " + testScenario + " case.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/update");
		SuccessMessage = "Successfully verified employee record update for " + testScenario + " case.";
		FailureMessage = "Could not verify record update with all valid details for " + testScenario + " case.";

             
        response = given().filter(new RequestLoggingFilter(requestCapture)).and().
        		header("contenetType", "application/json").and().body(employeeData).when().put("http://dummy.restapiexample.com/api/v1/update/" + employeeId);
        AssertionCheck("Status Code = 200", response.statusCode() + "", "200");
        try {
        	Thread.sleep(1000);
        } catch(Exception e) {
        	LOG.info("Interrupted while waiting after record update.");
        }
        
        if (testScenario.equals("Update Twice")) {
        	response = given().filter(new RequestLoggingFilter(requestCapture)).and().
            		header("contenetType", "application/json").and().body(employeeData).when().put("http://dummy.restapiexample.com/api/v1/update/" + employeeId);
            AssertionCheck("Status Code = 200", response.statusCode() + "", "200");
            
            try {
            	Thread.sleep(1000);
            } catch(Exception e) {
            	LOG.info("Interrupted while waiting after record update.");
            }
            
        }
        
              
        try {
        	AssertionCheck("Name = " + employeeName, response.body().jsonPath().get("name") + "", employeeName);
        	AssertionCheck("Salary = " + employeeSalary, response.body().jsonPath().get("salary") + "", employeeSalary);
        	AssertionCheck("Age = " + employeeAge, response.body().jsonPath().get("age") + "", employeeAge);
        	origEmployeeName = response.body().jsonPath().get("name") + "";
        	origEmployeeSalary = response.body().jsonPath().get("salary") + "";
        	origEmployeeAge = response.body().jsonPath().get("age") + "";
        } catch(JsonPathException e) {
        	reportLog.log(LogStatus.FAIL,  "Failed to parse JSON response. Response body does not contain JSON data.");
        	Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
        }
	}

	@Test(enabled=true, dataProvider="NegativeDataProvider")
	public void UpdateEmployeeDataNegativeTest(String testScenario, String errMsg, String employeeData) {
		LOG.info("Running Scenario: " + testScenario);
		String localEmployeeId = (testScenario.equals("Update Non-existent Record")) ? "1" : employeeId;
		reportLog.setDescription("Verify a new employee record is not updated for " + testScenario + " case.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/update");
		SuccessMessage = "Successfully verified record is not updated for employee for " + testScenario + " case.";
		FailureMessage = "Could not verify API behavior when updating a record for " + testScenario + " case.";

             
        response = given().filter(new RequestLoggingFilter(requestCapture)).and().
        		header("contenetType", "application/json").and().body(employeeData).when().put("http://dummy.restapiexample.com/api/v1/update/" + localEmployeeId);
        
        AssertionCheck("Status Code = 200", response.statusCode() + "", "200");
        AssertionCheck("Response body contains '" + errMsg + "'", response.body().asString().contains(errMsg) + "", "true");
	}
	
	
	
	@DataProvider(name="PositiveDataProvider")
	public Object[][] SetPositiveData(){
		
		return new Object[][] { {"Update Name Only", "TestUser" + System.currentTimeMillis() + Math.random(), origEmployeeSalary, origEmployeeAge},		 	
			{"Update Salary Only", origEmployeeName, "400000", origEmployeeAge},            		
			{"Update Age Only", origEmployeeName, origEmployeeSalary, "120"},          		
			{"Update Name and Salary", "TestUser" + System.currentTimeMillis() + Math.random(), "350000", origEmployeeAge},             	   	
			{"Update Name and Age", "TestUser" + System.currentTimeMillis() + Math.random(), origEmployeeSalary, "90"},       		
			{"Update Salary and Age", origEmployeeName, "8000.20", "80"},	
			{"Update Name, Salary and Age", "TestUser" + System.currentTimeMillis() + Math.random(), "500000", "50"},          	
			{"Update Same Record Twice", "TestUser" + System.currentTimeMillis() + Math.random(), "700000", "70"}
			
		};
	}
	
	@DataProvider(name="NegativeDataProvider")
	public Object[][] SetNegativeData(){
		return new Object[][] { 
			{"Update Non-existent Record", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"3000000\", \"age\" : \"30\"}"}, 																				
			{"Update ID Value", "error", "{\"id\" : \"2000\"}"}, 				
			{"Update Without Any Data", "error", "{}"},
			{"Update With Extra Attribute", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"department\" : \"sales\", \"salary\" : \"3000000\", \"age\" : \"30\"}"}, 																				// extra attribute
			{"Update With Misspelled Attribute", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"celery\" : \"3000000\", \"age\" : \"30\"}"},
			{"Update With Syntax Errors","error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + ",\"salary\" \"3000000\", \"age\" \"30\"}"},
			{"Update With Blank Value For Salary", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"\", \"age\" : \"30\"}"},
			{"Update With Negative Value For Salaray", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"-2000\", \"age\" : \"30\"}"},
			{"Update With Alphanumeric Value For Salary", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"abc\", \"age\" : \"30\"}"},
			{"Update With Blank Value For Age", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"\"}"},
			{"Update With Negative Value For Age", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"-20\"}"},
			{"Update With Floating Point Value For Age", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"13.5\"}"},
			{"Update With Alphanumeric Value For Age", "error", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"pqr\"}"}
							
		};
	}
	
}
