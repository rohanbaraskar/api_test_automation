/**
 * 
 */
package com.abacus.dummyapi;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.PrintStream;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.output.WriterOutputStream;
import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.response.Response;
/**
 * @author tm0338
 *
 */
public class BaseTest {
	
	protected ExtentReports report = null;
	public ExtentTest reportLog;
	protected String SuccessMessage="";
	protected String FailureMessage="";
	
	
	public StringWriter requestWriter = null;
	public PrintStream requestCapture = null;
	public Response response = null;
	
	private static final Logger LOG = Logger.getLogger(BaseTest.class);
	
	@BeforeMethod
	public void testStarter(Method method,Object[] testArgs){
		String testScenario = "";
		if (testArgs.length > 0) { testScenario = " :: " + testArgs[0].toString(); }
		String testName = method.getName() + testScenario;
		reportLog = report.startTest(testName,method.getName());
		SuccessMessage = "";
		FailureMessage = "";
		requestWriter = new StringWriter();
		requestCapture = new PrintStream(new WriterOutputStream(requestWriter,"UTF-8"));
	}
	
	public Response CreateRecord(String testScenario, String employeeName, String employeeSalary, String employeeAge) {
		requestWriter = new StringWriter();
		requestCapture = new PrintStream(new WriterOutputStream(requestWriter,"UTF-8"));
		String employeeData = "{\"name\" : \"" + employeeName + "\", \"salary\" : \"" + employeeSalary + "\" , \"age\" : \"" + employeeAge + "\"}";
		response = given().filter(new RequestLoggingFilter(requestCapture)).and().
        		header("contenetType", "application/json").and().body(employeeData).when().post("http://dummy.restapiexample.com/api/v1/create");
		return response;
	}
	
	
	public void AssertionCheck(String AssertionMessage, String Value1, String Value2) {
		try {
			assertEquals(Value1, Value2);
			reportLog.log(LogStatus.INFO,  "Successfully verified " + AssertionMessage);
		}catch(AssertionError ae) {
			reportLog.log(LogStatus.FAIL, "Could not verify " + AssertionMessage + "<br><br>" + ae.getMessage().replaceAll("\n", "<br>"));
			Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
		}
		
	}
	
	
	@AfterMethod
    public void TestStopper(ITestResult result,Object[] testArgs){
		requestCapture.flush();
		reportLog.log(LogStatus.INFO,  "<b>Request:</b><br><br> " + requestWriter.toString().replaceAll("\n", "<br>"));
		reportLog.log(LogStatus.INFO,  "Response Time: " + response.getTimeIn(TimeUnit.MILLISECONDS) + " ms");
		String responseString = response.asString().replaceAll("},",  "},<br>");
		if (responseString.contains("<html>")) { responseString = "<pre>" + responseString.replaceAll("<", "&lt;").replaceAll(">", "&gt;") + "</pre>";}
        reportLog.log(LogStatus.INFO,  "<b>Response:</b><br><br>" + responseString);
    	
        if (result.getStatus() == ITestResult.FAILURE) {
    		FailureMessage = FailureMessage.replaceAll("\n", "<br>").replaceAll("\t", "&emsp;");
    		reportLog.log(LogStatus.FAIL, "<b>Error!</b><br><br>" + FailureMessage);
    	} else {
    		SuccessMessage = SuccessMessage.replaceAll("\n", "<br>").replaceAll("\t", "&emsp;");
    		reportLog.log(LogStatus.PASS, SuccessMessage);
    	}
    	
    	
    	report.endTest(reportLog);
    	report.flush();
    	
    	try {
    		//Waiting five seconds before running next scenario
    		Thread.sleep(5000);
    	} catch(InterruptedException ie) {
    		LOG.error("Sleep interrupted while waiting after a test run.");
    	}
    	

	}
}
