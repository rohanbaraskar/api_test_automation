/**
 * 
 */
package com.abacus.dummyapi;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertTrue;

import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.response.Response;


/**
 * @author tejas_marathe
 * This class contains test cases for Delete operations on Dummy API
 * Following tests are covered:
 * Positive Tests
 * 1. Delete an existing records with following data
 * Negative Tests
 * 1. Attempt to delete a non-existent record
 * 
 * It uses @BeforeMethod and @AfterMethod (inherited from BaseTest) and @BeforeClass TestNG annotations to set up HTML reporting using ExtentReports
 * Each test method annotated by @Test contains test description, success and failure messages, api calls and assertions
 */


public class DeleteTests extends BaseTest {
	private String employeeId = "";
	private static final Logger LOG = Logger.getLogger(DeleteTests.class);
	
	@BeforeClass
	public void SetupUpdateTestCases() {
		report = new ExtentReports(Paths.get("").toAbsolutePath().toString() + "\\src\\test\\resources\\results\\APIDeleteTestResults.html");
		LOG.debug("Creating initial record before running delete test cases.");
		Response CreateEmployeeResponse = CreateRecord("Delete Test Values", "TestUser" + System.currentTimeMillis() + Math.random(), "1000", "100");
		employeeId = CreateEmployeeResponse.body().jsonPath().getString("id").toString();
		LOG.info("Employee ID: " + employeeId);
		if (employeeId.equals("")) {
			reportLog.log(LogStatus.FATAL, "Could not create employee before starting delete operation test cases. Exiting...");
			assertTrue(false);
		}
	}
	
	@Test
	public void DeleteEmployeeTest() {
		reportLog.setDescription("Verify employee record is delete successfully.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/delete/{id}");
		SuccessMessage = "Successfully verified employee record deleted.";
		FailureMessage = "Could not verify record delete.";

		LOG.debug("Creating initial record before running update test cases.");
				
        response = given().filter(new RequestLoggingFilter(requestCapture)).
        		when().delete("http://dummy.restapiexample.com/api/v1/delete/" + employeeId);
        AssertionCheck("Status Code = 200", response.statusCode() + "", "200");
        AssertionCheck("Delete Message = {\"success\":{\"text\":\"successfully! deleted Records\"}}", response.body().asString(), "{\"success\":{\"text\":\"successfully! deleted Records\"}}");
	}

	@Test(dependsOnMethods="DeleteEmployeeTest")
	public void DeleteEmployeeDataNegativeTest() {
		
		reportLog.setDescription("Verify error is returned when attempt made to delete non-existent employee.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/delete/{id}");
		SuccessMessage = "Successfully verified error is returned when attempt made to delete non-existent employee.";
		FailureMessage = "Could not verify API behavior when deleting a non-existent employee.";

             
        response = given().filter(new RequestLoggingFilter(requestCapture)).
        		when().delete("http://dummy.restapiexample.com/api/v1/delete/" + employeeId);
        
        AssertionCheck("Status Code = 200", response.statusCode() + "", "200");
        AssertionCheck("Delete Message = {\"success\":{\"text\":\"successfully! deleted Records\"}}", response.body().asString(), "{\"success\":{\"text\":\"successfully! deleted Records\"}}");
	}
	
}
