/**
 * 
 */
package com.abacus.dummyapi;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.filter.log.RequestLoggingFilter;


/**
 * @author tejas_marathe
 * This class contains test cases for Read operations on Dummy API
 * Following tests are covered:
 * Positive Tests
 * 1. Get data for all employees
 * 2. Get data for single existing employee
 * Negative Tests
 * 1. Attempt to get data from incomplete URL
 * 2. Attempt to get data for non-existing employee
 * 3. Attempt to get data for invalid employee id
 * 
 * It uses @BeforeMethod and @AfterMethod (inherited from BaseTest) and @BeforeClass TestNG annotations to set up HTML reporting using ExtentReports
 * Each test method annotated by @Test contains test description, success and failure messages, api calls and assertions
 */
public class ReadTests extends BaseTest{
	
	public static final Logger LOG = Logger.getLogger(ReadTests.class);
	
	@BeforeClass
	public void setupReadTestCases() {
		report = new ExtentReports(Paths.get("").toAbsolutePath().toString() + "\\src\\test\\resources\\results\\APIReadTestResults.html");
	}
	
	@Test(enabled=true)
	public void GetAllEmployeesDataTest() {
		reportLog.setDescription("Verify all employee records are returned successfully.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/employees");
		SuccessMessage = "Successfully verified retreiving data for all employees.";
		FailureMessage = "Could not verify retreival of data for all employees.";

        response = given().filter(new RequestLoggingFilter(requestCapture)).and().baseUri("http://dummy.restapiexample.com").and().basePath("/api/v1/employees").when().get();
        assertEquals(response.statusCode(), 200);
       
	}

	@Test(enabled=true)
	public void InvalidURLNegativeTest() {
		reportLog.setDescription("Verify employee records are not returned for invalid URL.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/employee");
		SuccessMessage = "Successfully verified no records are returned.";
		FailureMessage = "Could not verify API operation when incorrect URL provided.";

        response = given().filter(new RequestLoggingFilter(requestCapture)).and().baseUri("http://dummy.restapiexample.com").and().basePath("/api/v1/employee").when().get();
        assertEquals(response.statusCode(),404);

	}
	
	@Test(enabled=true, dataProvider="ExistingEmployeeDataProvider")
	public void GetEmployeeDataTest(String employeeData) {
		//Get employee data
		String id = employeeData.split(",")[0];
		String name = employeeData.split(",")[1];
		String salary = employeeData.split(",")[2];
		String age = employeeData.split(",")[3];
		String profileImage = employeeData.split(",")[4];
		
		reportLog.setDescription("Verify specified employee's data is returned successfully.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/employee/" + id);
		SuccessMessage = "Successfully verified single employee data is returned.";
		FailureMessage = "Could not verify retreival of data for single employee.";

        response = given().filter(new RequestLoggingFilter(requestCapture)).and().baseUri("http://dummy.restapiexample.com").and().basePath("/api/v1/employee/" + id).when().get();

    	assertEquals(response.statusCode(), 200);
    	try {
	    	assertEquals(response.body().jsonPath().get("id"),id);
	        assertEquals(response.body().jsonPath().get("employee_name"),name);
	        assertEquals(response.body().jsonPath().get("employee_salary"),salary);
	        assertEquals(response.body().jsonPath().get("employee_age"),age);
	        assertEquals(response.body().jsonPath().get("profile_image"),profileImage);
    	} catch(AssertionError ae) {
    		FailureMessage = FailureMessage + "<br>Expected data:<br>" + employeeData;
    	}
	}
	
	@Test(enabled=true, dataProvider="NonExistentEmployeeIDProvider")
	public void NonExistentEmployeeIdTest(String id) {
		reportLog.setDescription("Verify no data returned when non-existent employee id is provided.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/employee/" + id);
		SuccessMessage = "Successfully verified no data returned for employee id = " + id + ".";
		FailureMessage = "Could not verify API behavior when non-existent employee id provided.";

        response = given().filter(new RequestLoggingFilter(requestCapture)).and().baseUri("http://dummy.restapiexample.com").and().basePath("/api/v1/employee/" + id).when().get();

    	assertEquals(response.statusCode(), 200);
    	assertEquals(response.body().asString(),"false");
	}
	
	@Test(enabled=true, dataProvider="InvalidEmployeeIDProvider")
	public void InvalidEmployeeIdTest(String id) {
		reportLog.setDescription("Verify no data returned when invalid employee id is provided.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/employee/" + id);
		SuccessMessage = "Successfully verified no data returned for employee id = " + id + ".";
		FailureMessage = "Could not verify API behavior when invalid employee id provided.";

        response = given().filter(new RequestLoggingFilter(requestCapture)).and().baseUri("http://dummy.restapiexample.com").and().basePath("/api/v1/employee/" + id).when().get();

    	assertEquals(response.statusCode(), 200);
    	assertTrue(response.body().asString().contains("error"));
	}

	
	@DataProvider(name="ExistingEmployeeDataProvider")
	public Object[][] SetExistingEmployeeData(){
		return new Object[][] {{"110680,arun,1253653,28,:"}, {"110628,test51bfoooo,123,23,:"}};
	}
	
	@DataProvider(name="NonExistentEmployeeIDProvider")
	public Object[][] SetNonExistentEmployeeData(){
		return new Object[][] {{"1"}, {"123"}, {"0000"}};
	}
	
	@DataProvider(name="InvalidEmployeeIDProvider")
	public Object[][] SetInvalidEmployeeData(){
		return new Object[][] {{"PQR"}, {"1abc"}, {"1!%34$"}};
	}
	
}
